from django.urls import path, re_path
from . import views

app_name = 'music'

urlpatterns = [
    # /music/
    path(r'', views.IndexView.as_view(), name='index'),

    # /music/<album_id>/
    re_path(r'^(?P<pk>\d+)/$', views.DetailView.as_view(), name='detail'),

    # /music/album/add/
    path(r'album/add/$', views.AlbumCreate.as_view(), name='album-add'),



]